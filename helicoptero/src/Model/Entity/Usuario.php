<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;


/**
 * Usuario Entity
 *
 * @property int $id_usuario
 * @property int $id_rol
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property \Cake\I18n\FrozenDate $fecha_nacmiento
 * @property string $correo_electronico
 * @property string $clave
 */
class Usuario extends Entity
{
	 protected function _setClave($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
	
    protected $_accessible = [
        '*' => true,
        'id_usuario' => false
    ];
}
