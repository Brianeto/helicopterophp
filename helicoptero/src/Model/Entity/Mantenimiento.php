<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mantenimiento Entity
 *
 * @property int $id_mantenimiento
 * @property int $id_helicoptero
 * @property int $tipo_mantenimiento
 * @property int $estado_mantenimiento
 * @property \Cake\I18n\FrozenDate $fecha_inicio
 */
class Mantenimiento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id_mantenimiento' => false
    ];
}
