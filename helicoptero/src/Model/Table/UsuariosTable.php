<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Usuarios Model
 *
 * @method \App\Model\Entity\Usuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Usuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario findOrCreate($search, callable $callback = null, $options = [])
 */
class UsuariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('usuarios');
        $this->setDisplayField('id_usuario');
        $this->setPrimaryKey('id_usuario');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_usuario')
            ->allowEmpty('id_usuario', 'create');

        $validator
            ->integer('id_rol')
            ->requirePresence('id_rol', 'create')
            ->notEmpty('id_rol');

        $validator
            ->requirePresence('primer_nombre', 'create')
            ->notEmpty('primer_nombre');

        $validator
            ->allowEmpty('segundo_nombre');

        $validator
            ->requirePresence('primer_apellido', 'create')
            ->notEmpty('primer_apellido');

        $validator
            ->allowEmpty('segundo_apellido');

        $validator
            ->date('fecha_nacmiento')
            ->requirePresence('fecha_nacmiento', 'create')
            ->notEmpty('fecha_nacmiento');

        $validator
            ->requirePresence('correo_electronico', 'create')
            ->notEmpty('correo_electronico')
            ->add('correo_electronico', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('clave', 'create')
            ->notEmpty('clave');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['correo_electronico']));

        return $rules;
    }
}
