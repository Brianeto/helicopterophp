<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RolesPermisos Model
 *
 * @method \App\Model\Entity\RolesPermiso get($primaryKey, $options = [])
 * @method \App\Model\Entity\RolesPermiso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RolesPermiso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RolesPermiso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RolesPermiso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RolesPermiso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RolesPermiso findOrCreate($search, callable $callback = null, $options = [])
 */
class RolesPermisosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('roles_permisos');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_rol')
            ->requirePresence('id_rol', 'create')
            ->notEmpty('id_rol');

        $validator
            ->integer('id_permiso')
            ->requirePresence('id_permiso', 'create')
            ->notEmpty('id_permiso');

        return $validator;
    }
}
