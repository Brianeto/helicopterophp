<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Permisos Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Roles
 *
 * @method \App\Model\Entity\Permiso get($primaryKey, $options = [])
 * @method \App\Model\Entity\Permiso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Permiso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Permiso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Permiso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Permiso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Permiso findOrCreate($search, callable $callback = null, $options = [])
 */
class PermisosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('permisos');
        $this->setDisplayField('id_permiso');
        $this->setPrimaryKey('id_permiso');

        $this->belongsToMany('Roles', [
            'foreignKey' => 'permiso_id',
            'targetForeignKey' => 'role_id',
            'joinTable' => 'roles_permisos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_permiso')
            ->allowEmpty('id_permiso', 'create');

        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        return $validator;
    }
}
