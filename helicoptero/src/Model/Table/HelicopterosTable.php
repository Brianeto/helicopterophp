<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Helicopteros Model
 *
 * @method \App\Model\Entity\Helicoptero get($primaryKey, $options = [])
 * @method \App\Model\Entity\Helicoptero newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Helicoptero[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Helicoptero|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Helicoptero patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Helicoptero[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Helicoptero findOrCreate($search, callable $callback = null, $options = [])
 */
class HelicopterosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('helicopteros');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('modelo', 'create')
            ->notEmpty('modelo');

        $validator
            ->requirePresence('color', 'create')
            ->notEmpty('color');

        $validator
            ->requirePresence('matricula', 'create')
            ->notEmpty('matricula')
            ->add('matricula', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('numero_serie', 'create')
            ->notEmpty('numero_serie')
            ->add('numero_serie', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['matricula']));
        $rules->add($rules->isUnique(['numero_serie']));

        return $rules;
    }
}
