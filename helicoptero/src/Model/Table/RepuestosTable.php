<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Repuestos Model
 *
 * @method \App\Model\Entity\Repuesto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Repuesto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Repuesto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Repuesto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Repuesto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Repuesto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Repuesto findOrCreate($search, callable $callback = null, $options = [])
 */
class RepuestosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('repuestos');
        $this->setDisplayField('id_repuesto');
        $this->setPrimaryKey('id_repuesto');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_repuesto')
            ->allowEmpty('id_repuesto', 'create');

        $validator
            ->requirePresence('nombre_repuesto', 'create')
            ->notEmpty('nombre_repuesto')
            ->add('nombre_repuesto', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['nombre_repuesto']));

        return $rules;
    }
}
