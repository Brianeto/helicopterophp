<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Mantenimientos Model
 *
 * @method \App\Model\Entity\Mantenimiento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mantenimiento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mantenimiento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mantenimiento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mantenimiento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mantenimiento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mantenimiento findOrCreate($search, callable $callback = null, $options = [])
 */
class MantenimientosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('mantenimientos');
        $this->setDisplayField('id_mantenimiento');
        $this->setPrimaryKey('id_mantenimiento');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_mantenimiento')
            ->allowEmpty('id_mantenimiento', 'create');

        $validator
            ->integer('id_helicoptero')
            ->requirePresence('id_helicoptero', 'create')
            ->notEmpty('id_helicoptero');

        $validator
            ->integer('tipo_mantenimiento')
            ->requirePresence('tipo_mantenimiento', 'create')
            ->notEmpty('tipo_mantenimiento');

        $validator
            ->integer('estado_mantenimiento')
            ->requirePresence('estado_mantenimiento', 'create')
            ->notEmpty('estado_mantenimiento');

        $validator
            ->date('fecha_inicio')
            ->requirePresence('fecha_inicio', 'create')
            ->notEmpty('fecha_inicio');

        return $validator;
    }
}
