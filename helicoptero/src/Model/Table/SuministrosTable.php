<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Suministros Model
 *
 * @method \App\Model\Entity\Suministro get($primaryKey, $options = [])
 * @method \App\Model\Entity\Suministro newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Suministro[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Suministro|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Suministro patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Suministro[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Suministro findOrCreate($search, callable $callback = null, $options = [])
 */
class SuministrosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('suministros');
        $this->setDisplayField('id_suministro');
        $this->setPrimaryKey('id_suministro');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_suministro')
            ->allowEmpty('id_suministro', 'create');

        $validator
            ->requirePresence('nombre', 'create')
            ->notEmpty('nombre');

        return $validator;
    }
}
