<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Mantenimiento'), ['action' => 'edit', $mantenimiento->id_mantenimiento]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Mantenimiento'), ['action' => 'delete', $mantenimiento->id_mantenimiento], ['confirm' => __('Are you sure you want to delete # {0}?', $mantenimiento->id_mantenimiento)]) ?> </li>
        <li><?= $this->Html->link(__('List Mantenimientos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mantenimiento'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="mantenimientos view large-9 medium-8 columns content">
    <h3><?= h($mantenimiento->id_mantenimiento) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id Mantenimiento') ?></th>
            <td><?= $this->Number->format($mantenimiento->id_mantenimiento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Helicoptero') ?></th>
            <td><?= $this->Number->format($mantenimiento->id_helicoptero) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo Mantenimiento') ?></th>
            <td><?= $this->Number->format($mantenimiento->tipo_mantenimiento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado Mantenimiento') ?></th>
            <td><?= $this->Number->format($mantenimiento->estado_mantenimiento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Inicio') ?></th>
            <td><?= h($mantenimiento->fecha_inicio) ?></td>
        </tr>
    </table>
</div>
