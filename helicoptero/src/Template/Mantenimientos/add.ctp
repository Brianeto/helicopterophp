<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Mantenimientos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="mantenimientos form large-9 medium-8 columns content">
    <?= $this->Form->create($mantenimiento) ?>
    <fieldset>
        <legend><?= __('Add Mantenimiento') ?></legend>
        <?php
            echo $this->Form->control('id_helicoptero');
            echo $this->Form->control('tipo_mantenimiento');
            echo $this->Form->control('estado_mantenimiento');
            echo $this->Form->control('fecha_inicio');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
