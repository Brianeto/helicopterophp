<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mantenimiento'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="mantenimientos index large-9 medium-8 columns content">
    <h3><?= __('Mantenimientos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id_mantenimiento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id_helicoptero') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tipo_mantenimiento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('estado_mantenimiento') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha_inicio') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mantenimientos as $mantenimiento): ?>
            <tr>
                <td><?= $this->Number->format($mantenimiento->id_mantenimiento) ?></td>
                <td><?= $this->Number->format($mantenimiento->id_helicoptero) ?></td>
                <td><?= $this->Number->format($mantenimiento->tipo_mantenimiento) ?></td>
                <td><?= $this->Number->format($mantenimiento->estado_mantenimiento) ?></td>
                <td><?= h($mantenimiento->fecha_inicio) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mantenimiento->id_mantenimiento]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mantenimiento->id_mantenimiento]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mantenimiento->id_mantenimiento], ['confirm' => __('Are you sure you want to delete # {0}?', $mantenimiento->id_mantenimiento)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
