<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Suministro'), ['action' => 'edit', $suministro->id_suministro]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Suministro'), ['action' => 'delete', $suministro->id_suministro], ['confirm' => __('Are you sure you want to delete # {0}?', $suministro->id_suministro)]) ?> </li>
        <li><?= $this->Html->link(__('List Suministros'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Suministro'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="suministros view large-9 medium-8 columns content">
    <h3><?= h($suministro->id_suministro) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($suministro->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Suministro') ?></th>
            <td><?= $this->Number->format($suministro->id_suministro) ?></td>
        </tr>
    </table>
</div>
