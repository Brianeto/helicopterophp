<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $suministro->id_suministro],
                ['confirm' => __('Are you sure you want to delete # {0}?', $suministro->id_suministro)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Suministros'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="suministros form large-9 medium-8 columns content">
    <?= $this->Form->create($suministro) ?>
    <fieldset>
        <legend><?= __('Edit Suministro') ?></legend>
        <?php
            echo $this->Form->control('nombre');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
