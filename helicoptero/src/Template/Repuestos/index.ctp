<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Repuesto'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="repuestos index large-9 medium-8 columns content">
    <h3><?= __('Repuestos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id_repuesto') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre_repuesto') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($repuestos as $repuesto): ?>
            <tr>
                <td><?= $this->Number->format($repuesto->id_repuesto) ?></td>
                <td><?= h($repuesto->nombre_repuesto) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $repuesto->id_repuesto]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $repuesto->id_repuesto]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $repuesto->id_repuesto], ['confirm' => __('Are you sure you want to delete # {0}?', $repuesto->id_repuesto)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
