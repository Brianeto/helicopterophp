<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Repuesto'), ['action' => 'edit', $repuesto->id_repuesto]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Repuesto'), ['action' => 'delete', $repuesto->id_repuesto], ['confirm' => __('Are you sure you want to delete # {0}?', $repuesto->id_repuesto)]) ?> </li>
        <li><?= $this->Html->link(__('List Repuestos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Repuesto'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="repuestos view large-9 medium-8 columns content">
    <h3><?= h($repuesto->id_repuesto) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre Repuesto') ?></th>
            <td><?= h($repuesto->nombre_repuesto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Repuesto') ?></th>
            <td><?= $this->Number->format($repuesto->id_repuesto) ?></td>
        </tr>
    </table>
</div>
