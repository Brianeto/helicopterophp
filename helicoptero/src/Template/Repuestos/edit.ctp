<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $repuesto->id_repuesto],
                ['confirm' => __('Are you sure you want to delete # {0}?', $repuesto->id_repuesto)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Repuestos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="repuestos form large-9 medium-8 columns content">
    <?= $this->Form->create($repuesto) ?>
    <fieldset>
        <legend><?= __('Edit Repuesto') ?></legend>
        <?php
            echo $this->Form->control('nombre_repuesto');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
