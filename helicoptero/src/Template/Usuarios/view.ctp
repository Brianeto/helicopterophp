<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Usuario'), ['action' => 'edit', $usuario->id_usuario]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Usuario'), ['action' => 'delete', $usuario->id_usuario], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->id_usuario)]) ?> </li>
        <li><?= $this->Html->link(__('List Usuarios'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Usuario'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usuarios view large-9 medium-8 columns content">
    <h3><?= h($usuario->id_usuario) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Primer Nombre') ?></th>
            <td><?= h($usuario->primer_nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Segundo Nombre') ?></th>
            <td><?= h($usuario->segundo_nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Primer Apellido') ?></th>
            <td><?= h($usuario->primer_apellido) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Segundo Apellido') ?></th>
            <td><?= h($usuario->segundo_apellido) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Correo Electronico') ?></th>
            <td><?= h($usuario->correo_electronico) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Usuario') ?></th>
            <td><?= $this->Number->format($usuario->id_usuario) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Rol') ?></th>
            <td><?= $this->Number->format($usuario->id_rol) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Nacmiento') ?></th>
            <td><?= h($usuario->fecha_nacmiento) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Clave') ?></h4>
        <?= $this->Text->autoParagraph(h($usuario->clave)); ?>
    </div>
</div>
