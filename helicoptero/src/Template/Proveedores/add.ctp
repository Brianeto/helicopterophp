<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Proveedores'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="proveedores form large-9 medium-8 columns content">
    <?= $this->Form->create($proveedore) ?>
    <fieldset>
        <legend><?= __('Add Proveedore') ?></legend>
        <?php
            echo $this->Form->control('nombre_proveedor');
            echo $this->Form->control('direccion');
            echo $this->Form->control('correo_electronico');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
