<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Proveedore'), ['action' => 'edit', $proveedore->id_proveedor]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Proveedore'), ['action' => 'delete', $proveedore->id_proveedor], ['confirm' => __('Are you sure you want to delete # {0}?', $proveedore->id_proveedor)]) ?> </li>
        <li><?= $this->Html->link(__('List Proveedores'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Proveedore'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="proveedores view large-9 medium-8 columns content">
    <h3><?= h($proveedore->id_proveedor) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre Proveedor') ?></th>
            <td><?= h($proveedore->nombre_proveedor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Direccion') ?></th>
            <td><?= h($proveedore->direccion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Correo Electronico') ?></th>
            <td><?= h($proveedore->correo_electronico) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Proveedor') ?></th>
            <td><?= $this->Number->format($proveedore->id_proveedor) ?></td>
        </tr>
    </table>
</div>
