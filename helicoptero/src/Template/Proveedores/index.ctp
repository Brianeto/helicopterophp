<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Proveedore'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="proveedores index large-9 medium-8 columns content">
    <h3><?= __('Proveedores') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id_proveedor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre_proveedor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('direccion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('correo_electronico') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($proveedores as $proveedore): ?>
            <tr>
                <td><?= $this->Number->format($proveedore->id_proveedor) ?></td>
                <td><?= h($proveedore->nombre_proveedor) ?></td>
                <td><?= h($proveedore->direccion) ?></td>
                <td><?= h($proveedore->correo_electronico) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $proveedore->id_proveedor]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $proveedore->id_proveedor]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $proveedore->id_proveedor], ['confirm' => __('Are you sure you want to delete # {0}?', $proveedore->id_proveedor)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
