<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Helicoptero'), ['action' => 'edit', $helicoptero->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Helicoptero'), ['action' => 'delete', $helicoptero->id], ['confirm' => __('Are you sure you want to delete # {0}?', $helicoptero->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Helicopteros'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Helicoptero'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="helicopteros view large-9 medium-8 columns content">
    <h3><?= h($helicoptero->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Modelo') ?></th>
            <td><?= h($helicoptero->modelo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Color') ?></th>
            <td><?= h($helicoptero->color) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Matricula') ?></th>
            <td><?= h($helicoptero->matricula) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero Serie') ?></th>
            <td><?= h($helicoptero->numero_serie) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($helicoptero->id) ?></td>
        </tr>
    </table>
</div>
