<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Helicoptero'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="helicopteros index large-9 medium-8 columns content">
    <h3><?= __('Helicopteros') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modelo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('color') ?></th>
                <th scope="col"><?= $this->Paginator->sort('matricula') ?></th>
                <th scope="col"><?= $this->Paginator->sort('numero_serie') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($helicopteros as $helicoptero): ?>
            <tr>
                <td><?= $this->Number->format($helicoptero->id) ?></td>
                <td><?= h($helicoptero->modelo) ?></td>
                <td><?= h($helicoptero->color) ?></td>
                <td><?= h($helicoptero->matricula) ?></td>
                <td><?= h($helicoptero->numero_serie) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $helicoptero->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $helicoptero->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $helicoptero->id], ['confirm' => __('Are you sure you want to delete # {0}?', $helicoptero->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
