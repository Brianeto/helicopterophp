<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $helicoptero->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $helicoptero->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Helicopteros'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="helicopteros form large-9 medium-8 columns content">
    <?= $this->Form->create($helicoptero) ?>
    <fieldset>
        <legend><?= __('Edit Helicoptero') ?></legend>
        <?php
            echo $this->Form->control('modelo');
            echo $this->Form->control('color');
            echo $this->Form->control('matricula');
            echo $this->Form->control('numero_serie');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
