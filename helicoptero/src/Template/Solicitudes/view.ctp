<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Solicitude'), ['action' => 'edit', $solicitude->id_solicitud]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Solicitude'), ['action' => 'delete', $solicitude->id_solicitud], ['confirm' => __('Are you sure you want to delete # {0}?', $solicitude->id_solicitud)]) ?> </li>
        <li><?= $this->Html->link(__('List Solicitudes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Solicitude'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="solicitudes view large-9 medium-8 columns content">
    <h3><?= h($solicitude->id_solicitud) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id Solicitud') ?></th>
            <td><?= $this->Number->format($solicitude->id_solicitud) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Usuario') ?></th>
            <td><?= $this->Number->format($solicitude->id_usuario) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Estado Solicitud') ?></th>
            <td><?= $this->Number->format($solicitude->estado_solicitud) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Emision') ?></th>
            <td><?= h($solicitude->fecha_emision) ?></td>
        </tr>
    </table>
</div>
