<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $solicitude->id_solicitud],
                ['confirm' => __('Are you sure you want to delete # {0}?', $solicitude->id_solicitud)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Solicitudes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="solicitudes form large-9 medium-8 columns content">
    <?= $this->Form->create($solicitude) ?>
    <fieldset>
        <legend><?= __('Edit Solicitude') ?></legend>
        <?php
            echo $this->Form->control('id_usuario');
            echo $this->Form->control('estado_solicitud');
            echo $this->Form->control('fecha_emision');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
