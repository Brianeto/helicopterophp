<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Helicopteros Controller
 *
 * @property \App\Model\Table\HelicopterosTable $Helicopteros
 *
 * @method \App\Model\Entity\Helicoptero[] paginate($object = null, array $settings = [])
 */
class HelicopterosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $helicopteros = $this->paginate($this->Helicopteros);

        $this->set(compact('helicopteros'));
        $this->set('_serialize', ['helicopteros']);
    }

    /**
     * View method
     *
     * @param string|null $id Helicoptero id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $helicoptero = $this->Helicopteros->get($id, [
            'contain' => []
        ]);

        $this->set('helicoptero', $helicoptero);
        $this->set('_serialize', ['helicoptero']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $helicoptero = $this->Helicopteros->newEntity();
        if ($this->request->is('post')) {
            $helicoptero = $this->Helicopteros->patchEntity($helicoptero, $this->request->getData());
            if ($this->Helicopteros->save($helicoptero)) {
                $this->Flash->success(__('The helicoptero has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The helicoptero could not be saved. Please, try again.'));
        }
        $this->set(compact('helicoptero'));
        $this->set('_serialize', ['helicoptero']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Helicoptero id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $helicoptero = $this->Helicopteros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $helicoptero = $this->Helicopteros->patchEntity($helicoptero, $this->request->getData());
            if ($this->Helicopteros->save($helicoptero)) {
                $this->Flash->success(__('The helicoptero has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The helicoptero could not be saved. Please, try again.'));
        }
        $this->set(compact('helicoptero'));
        $this->set('_serialize', ['helicoptero']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Helicoptero id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $helicoptero = $this->Helicopteros->get($id);
        if ($this->Helicopteros->delete($helicoptero)) {
            $this->Flash->success(__('The helicoptero has been deleted.'));
        } else {
            $this->Flash->error(__('The helicoptero could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
