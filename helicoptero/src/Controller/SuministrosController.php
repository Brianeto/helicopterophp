<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Suministros Controller
 *
 * @property \App\Model\Table\SuministrosTable $Suministros
 *
 * @method \App\Model\Entity\Suministro[] paginate($object = null, array $settings = [])
 */
class SuministrosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $suministros = $this->paginate($this->Suministros);

        $this->set(compact('suministros'));
        $this->set('_serialize', ['suministros']);
    }

    /**
     * View method
     *
     * @param string|null $id Suministro id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $suministro = $this->Suministros->get($id, [
            'contain' => []
        ]);

        $this->set('suministro', $suministro);
        $this->set('_serialize', ['suministro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $suministro = $this->Suministros->newEntity();
        if ($this->request->is('post')) {
            $suministro = $this->Suministros->patchEntity($suministro, $this->request->getData());
            if ($this->Suministros->save($suministro)) {
                $this->Flash->success(__('The suministro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suministro could not be saved. Please, try again.'));
        }
        $this->set(compact('suministro'));
        $this->set('_serialize', ['suministro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Suministro id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $suministro = $this->Suministros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $suministro = $this->Suministros->patchEntity($suministro, $this->request->getData());
            if ($this->Suministros->save($suministro)) {
                $this->Flash->success(__('The suministro has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The suministro could not be saved. Please, try again.'));
        }
        $this->set(compact('suministro'));
        $this->set('_serialize', ['suministro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Suministro id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $suministro = $this->Suministros->get($id);
        if ($this->Suministros->delete($suministro)) {
            $this->Flash->success(__('The suministro has been deleted.'));
        } else {
            $this->Flash->error(__('The suministro could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
