<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RolesPermisos Controller
 *
 * @property \App\Model\Table\RolesPermisosTable $RolesPermisos
 *
 * @method \App\Model\Entity\RolesPermiso[] paginate($object = null, array $settings = [])
 */
class RolesPermisosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $rolesPermisos = $this->paginate($this->RolesPermisos);

        $this->set(compact('rolesPermisos'));
        $this->set('_serialize', ['rolesPermisos']);
    }

    /**
     * View method
     *
     * @param string|null $id Roles Permiso id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rolesPermiso = $this->RolesPermisos->get($id, [
            'contain' => []
        ]);

        $this->set('rolesPermiso', $rolesPermiso);
        $this->set('_serialize', ['rolesPermiso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $rolesPermiso = $this->RolesPermisos->newEntity();
        if ($this->request->is('post')) {
            $rolesPermiso = $this->RolesPermisos->patchEntity($rolesPermiso, $this->request->getData());
            if ($this->RolesPermisos->save($rolesPermiso)) {
                $this->Flash->success(__('The roles permiso has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The roles permiso could not be saved. Please, try again.'));
        }
        $this->set(compact('rolesPermiso'));
        $this->set('_serialize', ['rolesPermiso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Roles Permiso id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $rolesPermiso = $this->RolesPermisos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $rolesPermiso = $this->RolesPermisos->patchEntity($rolesPermiso, $this->request->getData());
            if ($this->RolesPermisos->save($rolesPermiso)) {
                $this->Flash->success(__('The roles permiso has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The roles permiso could not be saved. Please, try again.'));
        }
        $this->set(compact('rolesPermiso'));
        $this->set('_serialize', ['rolesPermiso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Roles Permiso id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $rolesPermiso = $this->RolesPermisos->get($id);
        if ($this->RolesPermisos->delete($rolesPermiso)) {
            $this->Flash->success(__('The roles permiso has been deleted.'));
        } else {
            $this->Flash->error(__('The roles permiso could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
