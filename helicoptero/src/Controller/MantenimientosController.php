<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Mantenimientos Controller
 *
 * @property \App\Model\Table\MantenimientosTable $Mantenimientos
 *
 * @method \App\Model\Entity\Mantenimiento[] paginate($object = null, array $settings = [])
 */
class MantenimientosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $mantenimientos = $this->paginate($this->Mantenimientos);

        $this->set(compact('mantenimientos'));
        $this->set('_serialize', ['mantenimientos']);

    }

    /**
     * View method
     *
     * @param string|null $id Mantenimiento id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mantenimiento = $this->Mantenimientos->get($id, [
            'contain' => []
        ]);

        $this->set('mantenimiento', $mantenimiento);
        $this->set('_serialize', ['mantenimiento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mantenimiento = $this->Mantenimientos->newEntity();
        if ($this->request->is('post')) {
            $mantenimiento = $this->Mantenimientos->patchEntity($mantenimiento, $this->request->getData());
            if ($this->Mantenimientos->save($mantenimiento)) {
                $this->Flash->success(__('The mantenimiento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mantenimiento could not be saved. Please, try again.'));
        }
        $this->set(compact('mantenimiento'));
        $this->set('_serialize', ['mantenimiento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Mantenimiento id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mantenimiento = $this->Mantenimientos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mantenimiento = $this->Mantenimientos->patchEntity($mantenimiento, $this->request->getData());
            if ($this->Mantenimientos->save($mantenimiento)) {
                $this->Flash->success(__('The mantenimiento has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mantenimiento could not be saved. Please, try again.'));
        }
        $this->set(compact('mantenimiento'));
        $this->set('_serialize', ['mantenimiento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mantenimiento id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mantenimiento = $this->Mantenimientos->get($id);
        if ($this->Mantenimientos->delete($mantenimiento)) {
            $this->Flash->success(__('The mantenimiento has been deleted.'));
        } else {
            $this->Flash->error(__('The mantenimiento could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
