<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RolesPermisosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RolesPermisosTable Test Case
 */
class RolesPermisosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RolesPermisosTable
     */
    public $RolesPermisos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.roles_permisos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RolesPermisos') ? [] : ['className' => 'App\Model\Table\RolesPermisosTable'];
        $this->RolesPermisos = TableRegistry::get('RolesPermisos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RolesPermisos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
