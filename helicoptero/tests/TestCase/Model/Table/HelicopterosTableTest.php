<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HelicopterosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HelicopterosTable Test Case
 */
class HelicopterosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HelicopterosTable
     */
    public $Helicopteros;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.helicopteros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Helicopteros') ? [] : ['className' => 'App\Model\Table\HelicopterosTable'];
        $this->Helicopteros = TableRegistry::get('Helicopteros', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Helicopteros);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
