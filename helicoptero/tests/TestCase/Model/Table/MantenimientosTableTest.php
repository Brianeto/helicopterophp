<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MantenimientosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MantenimientosTable Test Case
 */
class MantenimientosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MantenimientosTable
     */
    public $Mantenimientos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.mantenimientos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Mantenimientos') ? [] : ['className' => 'App\Model\Table\MantenimientosTable'];
        $this->Mantenimientos = TableRegistry::get('Mantenimientos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Mantenimientos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
