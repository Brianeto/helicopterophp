<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuministrosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuministrosTable Test Case
 */
class SuministrosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SuministrosTable
     */
    public $Suministros;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.suministros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Suministros') ? [] : ['className' => 'App\Model\Table\SuministrosTable'];
        $this->Suministros = TableRegistry::get('Suministros', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Suministros);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
