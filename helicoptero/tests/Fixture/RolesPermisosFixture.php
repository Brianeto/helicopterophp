<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RolesPermisosFixture
 *
 */
class RolesPermisosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id_rol' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'id_permiso' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'id_rol' => ['type' => 'index', 'columns' => ['id_rol'], 'length' => []],
            'id_permiso' => ['type' => 'index', 'columns' => ['id_permiso'], 'length' => []],
        ],
        '_constraints' => [
            'roles_permisos_ibfk_1' => ['type' => 'foreign', 'columns' => ['id_rol'], 'references' => ['roles', 'id_rol'], 'update' => 'restrict', 'delete' => 'cascade', 'length' => []],
            'roles_permisos_ibfk_2' => ['type' => 'foreign', 'columns' => ['id_permiso'], 'references' => ['permisos', 'id_permiso'], 'update' => 'restrict', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_spanish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id_rol' => 1,
            'id_permiso' => 1
        ],
    ];
}
